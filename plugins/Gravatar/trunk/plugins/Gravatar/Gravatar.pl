package MT::Plugin::Gravatar;

use strict;
use MT;
use base qw( MT::Plugin );
our $VERSION = '1.01';

use MT;

my $plugin = MT::Plugin::Gravatar->new({
	id          => 'Gravatar',
	key         => 'Gravatar',
	name        => 'Gravatar',
	description => "Get Gravatar Images",
	version     => $VERSION,
});

MT->add_plugin($plugin);

sub init_registry {
	my $plugin = shift;
	$plugin->registry({
		tags => {
			function => {
				'Gravatar' => \&_hdlr_Gravatar,
			},
		},
	});
}

sub _hdlr_Gravatar {
	use URI::Escape;
	use Digest::MD5 qw(md5_hex);
 
	my ($ctx, $args) = @_;
	my $entry = $ctx->stash('entry');
	my $comment = $ctx->stash('comment');
	my $author = $ctx->stash('author');
	my $email;
	if ($entry) {
		$email = $entry->author->email;
	}
	if ($comment) {
		$email = $comment->email;
	}
	if (!$comment && !$entry && $author) {
		$email = $author->email;
	}	

	my $url = "http://www.gravatar.com/avatar.php?gravatar_id=".md5_hex($email);
	$url .= exists $args->{rating} ? "&amp;rating=".$args->{rating} : "";
	$url .= exists $args->{size} ? "&amp;size=".$args->{size} : "";
	$url .= exists $args->{default} ? "&amp;default=".uri_escape($args->{default}) : "";
	$url .= exists $args->{border} ? "&amp;border=".$args->{border} : "";

	return $url;
}

1;
