package VWPass;

sub password_is_strong {
	my $password = shift;
	my $error_string = "";
	my @errors;
	
	# password must start with a letter
	unless ($password =~ /^[A-Za-z]/) {
		push(@errors, "Password must start with a letter.");
	}
	
	# password must contain one number
	unless ($password =~ /\d/) {
		push(@errors, "Password must contain one number.");
	}
	
	# password must contain one special character in ! @ # $ % ^ & * ( ) _ + - = [ ] { } ; ' : " , . / < > ?
	unless ($password =~ /[\!\@\#\$\%\^\&\*\(\)\_\+\-\=\[\]\{\}\;\'\:\"\,\.\/\<\>\?]/) {
		push(@errors, "Password must contain one special character.");
	}
	
	# password must be at least 8 characters long
	unless (scalar(split("",$password)) >= 8) { 
		push(@errors, "Password must be at least 8 characters long."); 
	}
	
	# if there are errors, show them
	if (@errors) {
		$error_string = join(" ", @errors);
	}
	
	return $error_string;
}

sub init_app {
	local $SIG{__WARN__} = sub {};
	
	require MT::App;
	my $orig_method1 = \&MT::App::create_user_pending;
	*MT::App::create_user_pending = sub {
		my ($app, $param) = @_;
		my $password = $app->param('pass') || $app->param('password');
		if ($password) {
			my $error_string = password_is_strong($password);
			die("$error_string") if ($error_string);
		}
		return $orig_method1->($app, $param);
	};

	require MT::CMS::User;
	my $orig_method2 = \&MT::CMS::User::save_filter;
	*MT::CMS::User::save_filter = sub {
		my ($eh, $app) = @_;
		
		if ( !$app->param('id') ) {    # it's a new object
			die("User requires password") if ( !$app->param('pass') );
		} else {
			my $password = $app->param('pass') || $app->param('password');
			if ($password) {
				my $error_string = password_is_strong($password);
				die("$error_string") if ($error_string);
			}
		}
		return $orig_method2->($eh, $app);
	};
	
	require MT::App::Community;
	my $orig_method3 = \&MT::App::Community::save_profile_method;	
	*MT::App::Community::save_profile_method = sub {
		my ($app) = @_;
		my $password = $app->param('pass') || $app->param('password');
		if ($password) {
			my $error_string = password_is_strong($password);
			die("$error_string") if ($error_string);			
		}
		return $orig_method3->($app);
	};
	
	require MT::Template::Context::Search;
	my $orig_method4 = \&MT::Template::Context::Search::_hdlr_results;	
	*MT::Template::Context::Search::_hdlr_results = sub {
		my($ctx, $args, $cond) = @_;

		## If there are no results, return the empty string, knowing
		## that the handler for <MTNoSearchResults> will fill in the
		## no results message.
		my $iter = $ctx->stash('results') or return '';
		my $count = $ctx->stash('count') or return '';
		my $max = $ctx->stash('maxresults');
		my $stash_key = $ctx->stash('stash_key') || 'entry';

		my $output = '';
		my $build = $ctx->stash('builder');
		my $tokens = $ctx->stash('tokens');
		my $blog_header = 1;
		my $blog_footer = 0;
		my $footer = 0;
		my $count_per_blog = 0;
		my $max_reached = 0;
		my ( $this_object, $next_object );
		$this_object = $iter->();
		return '' unless $this_object;
		for ( my $i = 0; $i < $count; $i++) {
			$count_per_blog++;
			#$ctx->stash($stash_key, $this_object);
			MT::log("HELLO $stash_key");
			local $ctx->{__stash}{$stash_key} = $this_object;
			local $ctx->{__stash}{blog} = $this_object->blog if $this_object->can('blog');
			my $ts;
			if ( $this_object->isa('MT::Entry') ) {
				$ts = $this_object->authored_on;
			}
			elsif ( $this_object->properties->{audit} ) {
				$ts = $this_object->created_on;
			}
			local $ctx->{current_timestamp} = $ts;
			if ( $next_object = $iter->() ) {
				if ( $next_object->can('blog') ) {
					$blog_footer = $next_object->blog_id ne $this_object->blog_id ? 1 : 0;
				}
			}
			else {
				$blog_footer = 1;
				$footer      = 1;
			}

			defined(my $out = $build->build($ctx, $tokens,
				{ %$cond, 
					SearchResultsHeader => $i == 0,
					SearchResultsFooter => $footer,
					BlogResultHeader => $blog_header,
					BlogResultFooter => $blog_footer,
					IfMaxResultsCutoff => $max_reached,
				} )) or return $ctx->error( $build->errstr );
			$output .= $out;

			$this_object = $next_object;
			last unless $this_object;
			$blog_header = $blog_footer ? 1 : 0;
		}
		$output;
	};
}

1;





