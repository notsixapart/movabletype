<?php
# Movable Type (r) Open Source (C) 2001-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: function.mttagcount.php 5225 2010-01-27 07:14:14Z takayama $

function smarty_function_mttagcount($args, &$ctx) {
    $tag = $ctx->stash('Tag');
    $count = 0;
    if ($tag && is_object($tag))
        $count = $tag->tag_count;
    return $ctx->count_format($count, $args);
}
?>
