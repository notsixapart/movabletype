<?php
# Movable Type (r) Open Source (C) 2001-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: function.mtwebsitetimezone.php 5151 2010-01-06 07:51:27Z takayama $

function smarty_function_mtwebsitetimezone($args, &$ctx) {
    // status: complete
    // parameters: no_colon
    require_once('function.mtblogtimezone.php');
    return smarty_function_mtblogtimezone($args, $ctx);
}
?>
