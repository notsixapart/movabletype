<?php
# Movable Type (r) Open Source (C) 2001-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: block.mtifexternalusermanagement.php 5151 2010-01-06 07:51:27Z takayama $

function smarty_block_mtifexternalusermanagement($args, $content, &$ctx, &$repeat) {
    return $ctx->_hdlr_if($args, $content, $ctx, $repeat, $ctx->mt->config('ExternalUserManagement'));
}
?>
