<?php
# Movable Type (r) Open Source (C) 2001-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: function.mtcategorytrackbackcount.php 5225 2010-01-27 07:14:14Z takayama $

function smarty_function_mtcategorytrackbackcount($args, &$ctx) {
    $cat = $ctx->stash('category');
    $cat_id = $cat->category_id;
    $count = $ctx->mt->db()->category_ping_count($cat_id);
    return $ctx->count_format($count, $args);
}
?>
