<?php
# Movable Type (r) Open Source (C) 2001-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: function.mtauthorbasename.php 5225 2010-01-27 07:14:14Z takayama $

function smarty_function_mtauthorbasename($args, &$ctx) {
    $author = $ctx->stash('author');
    if (!$author) return '';
    $basename = $author->author_basename;
    if ($sep = $args['separator']) {
        if ($sep == '-') {
            $basename = preg_replace('/_/', '-', $basename);
        } elseif ($sep == '_') {
            $basename = preg_replace('/-/', '_', $basename);
        }
    }
    return $basename;
}
?>
