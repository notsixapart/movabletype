<?php
# Movable Type (r) Open Source (C) 2001-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: function.mtcommentscorecount.php 5225 2010-01-27 07:14:14Z takayama $

require_once('rating_lib.php');

function smarty_function_mtcommentscorecount($args, &$ctx) {
    return hdlr_score_count($ctx, 'comment', $args['namespace'], $args);
}
?>
