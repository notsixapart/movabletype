<?php
# Movable Type (r) Open Source (C) 2001-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: function.mtwebsitepingcount.php 5225 2010-01-27 07:14:14Z takayama $

function smarty_function_mtwebsitepingcount($args, &$ctx) {
    require_once('function.mtblogpingcount.php');
    return smarty_function_mtblogpingcount($args, $ctx);
}
?>
