<?php
# Movable Type (r) Open Source (C) 2001-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: function.mtblogcategorycount.php 5225 2010-01-27 07:14:14Z takayama $

function smarty_function_mtblogcategorycount($args, &$ctx) {
    // status: complete
    // parameters: none
    $args['blog_id'] = $ctx->stash('blog_id');
    $count = $ctx->mt->db()->blog_category_count($args);
    return $ctx->count_format($count, $args);
}
?>
