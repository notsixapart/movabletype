<?php
# Movable Type (r) Open Source (C) 2001-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: function.mtpagetitle.php 5151 2010-01-06 07:51:27Z takayama $

require_once('function.mtentrytitle.php');
function smarty_function_mtpagetitle($args, &$ctx) {
    return smarty_function_mtentrytitle($args, $ctx);
}
?>
