<?php
# Movable Type (r) Open Source (C) 2001-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: block.mtpageiftagged.php 5367 2010-03-08 07:50:33Z asawada $

require_once('block.mtentryiftagged.php');
function smarty_block_mtpageiftagged($args, $content, &$ctx, &$repeat) {
    $args['class'] = 'page';
    return smarty_block_mtentryiftagged($args, $content, $ctx, $repeat);
}
?>
