# Movable Type (r) Open Source (C) 2005-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: L10N.pm 5151 2010-01-06 07:51:27Z takayama $

package StyleCatcher::L10N;
use strict;
use base 'MT::Plugin::L10N';

1;
