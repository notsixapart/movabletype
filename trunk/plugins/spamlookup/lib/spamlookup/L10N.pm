# Movable Type (r) Open Source (C) 2006-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: L10N.pm 5151 2010-01-06 07:51:27Z takayama $

# Original copyright (c) 2004-2006, Brad Choate and Tobias Hoellrich

package spamlookup::L10N;
use strict;

use MT::Plugin::L10N;
@spamlookup::L10N::ISA = qw(MT::Plugin::L10N);

1;
