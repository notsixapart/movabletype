# Movable Type (r) Open Source (C) 2001-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: en_us.pm 5151 2010-01-06 07:51:27Z takayama $

package MT::I18N::en_us;

use strict;
use vars qw(@ISA);
@ISA = qw( MT::I18N::default );

1;
