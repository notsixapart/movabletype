package BrownShorter::URL;
use strict;
use warnings;
use base qw( MT::Object );

__PACKAGE__->install_properties ({
	column_defs     => {
		'id' => 'integer not null primary key auto_increment',
		'entry_id' => 'integer',
		'short_url' => 'string(255)',
	},
	datasource => 'brownshorterurl',
	primary_key => 'id',
	audit => 1,
	indexes => {
		entry_id => 1,
		short_url => 1,
	},
});