package MT::Plugin::BrownShorter;

use strict;
use base qw( MT::Plugin );

use MT;
use MT::Util;
use MT::Blog;
use MT::Entry;

use vars qw( $VERSION $plugin);
$VERSION = '0.10';

$plugin = MT::Plugin::BrownShorter->new ({
	name        => 'BrownShorter',
	description => 'Shorten URLs.',
	version     => $VERSION,
	schema_version	=> $VERSION,
	object_classes	=> [ 'BrownShorter::URL' ],
	callbacks   => {
		'MT::Entry::post_save' => \&post_save_entry,
	},
	template_tags => {
		'EntryShortPermalink' => \&entry_short_permalink_tag,
	},
	settings    => new MT::PluginSettings([
		[ 'short_permalink_base_url', {Default => undef, Scope => 'blog'}],
	]),
	blog_config_template => \&blog_config,
});
MT->add_plugin($plugin);

sub instance {
	$plugin;
}

sub load_config {
	my $plugin = shift;
	$plugin->SUPER::load_config (@_);
	if ($_[1] =~ /^blog:(\d+)$/) {
		my $blog_id = $1;
		my $blog = MT::Blog->load($blog_id);
		unless ($_[0]->{short_permalink_base_url}) {
			$_[0]->{short_permalink_base_url} = "";
		}
	}
}

sub shorten_url {
	my ($entry) = @_;
	return '' unless ($entry);
	use BrownShorter::URL;
	my $bs = BrownShorter::URL->load({ entry_id => $entry->id });
	unless ($bs) {
		my $base_url = $entry->permalink;
		my $base_url = $plugin->get_config_value('short_permalink_base_url', 'blog:'.$entry->blog_id);
		return '' unless ($base_url);
		$base_url = $base_url . "/" unless ($base_url =~ /\/$/);
		my $api_url = = $base_url . "shortenurl.php?url=" . $base_url;
		my $result = LWP::Simple::get($url);
		$bs = BrownShorter::URL->new;
		$bs->short_url($result);
		$bs->save;
	}
	return $bs->short_url;
}

sub post_save_entry {
	my ($eh, $entry, $orig) = @_;
	my $short_url = shorten_url($entry);
	return $short_url;
}

sub entry_short_permalink_tag {
	my ($ctx, $args) = @_;
	my $entry = $ctx->stash('entry');
	my $short_url = shorten_url($entry);
	return $short_url;
}

sub blog_config {
	my $html = <<HTML;
<fieldset>
<div class="setting">
    <div class="label">
        <MT_TRANS phrase="Shortener Base URL: ">
    </div>
    <div class="field">
		<input type="text" size="40" name="short_permalink_base_url" id="short_permalink_base_url" value="<TMPL_VAR NAME="SHORT_PERMALINK_BASE_URL">">
        <p><MT_TRANS phrase="The shortener's base URL."></p>
    </div>
</div>
</fieldset>
HTML
}

1;




