<?php
# Movable Type (r) Open Source (C) 2001-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: modifier.words.php 5144 2010-01-06 05:49:46Z takayama $

function smarty_modifier_words($text, $words) {
    require_once("MTUtil.php");
    return first_n_text($text, $words);
}
?>
