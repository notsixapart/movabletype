<?php
# Movable Type (r) Open Source (C) 2001-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: function.mtnextlink.php 5144 2010-01-06 05:49:46Z takayama $

function smarty_function_mtnextlink($args, &$ctx) {

    $limit = $ctx->stash('__pager_limit');
    $offset = $ctx->stash('__pager_offset');
    $offset += $limit;

    if ( strpos($link, '?') ) {
        $link .= '&';
    }
    else {
        $link .= '?';
    }

    $link .= "limit=$limit";
    if ( $offset )
        $link .= "&offset=$offset";
    return $link;
}
?>

