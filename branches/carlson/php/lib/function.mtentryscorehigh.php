<?php
# Movable Type (r) Open Source (C) 2001-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: function.mtentryscorehigh.php 5144 2010-01-06 05:49:46Z takayama $

require_once('rating_lib.php');

function smarty_function_mtentryscorehigh($args, &$ctx) {
    return hdlr_score_high($ctx, 'entry', $args['namespace'], $args);
}
?>
