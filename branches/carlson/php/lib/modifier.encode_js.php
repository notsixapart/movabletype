<?php
# Movable Type (r) Open Source (C) 2001-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: modifier.encode_js.php 5144 2010-01-06 05:49:46Z takayama $

function smarty_modifier_encode_js($text) {
    require_once("MTUtil.php");
    return encode_js($text);
}
?>
