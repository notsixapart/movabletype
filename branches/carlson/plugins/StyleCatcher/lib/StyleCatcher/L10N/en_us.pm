# Movable Type (r) Open Source (C) 2005-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: en_us.pm 5144 2010-01-06 05:49:46Z takayama $

package StyleCatcher::L10N::en_us;

use strict;

use base 'StyleCatcher::L10N';
use vars qw( %Lexicon );
%Lexicon = ();

1;
