# Movable Type (r) Open Source (C) 2006-2010 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id: L10N.pm 5144 2010-01-06 05:49:46Z takayama $

# Original Copyright (c) 2004-2006 David Raynes

package MultiBlog::L10N;

use strict;
use base 'MT::Plugin::L10N';

1;
