#!/usr/bin/perl
# $Id: 43-localua.t 1098 2007-12-12 01:47:58Z hachi $
use strict;
use warnings;
use Test::More tests => 1;
use lib 't/lib';
use LWP::UserAgent::Local;
$ENV{CONFIG} = 't/mt.cfg';
my $ua = LWP::UserAgent::Local->new({ScriptAlias => '/'});
my $req = HTTP::Request->new(GET => 'http://localhost/mt-atom.cgi/weblog/blog_id=1');
my $resp = $ua->request($req);
print $resp->headers_as_string(), $resp->content();
ok($resp->content(), "$resp->content");
